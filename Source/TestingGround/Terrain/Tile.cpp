	// Fill out your copyright notice in the Description page of Project Settings.

#include "Tile.h"
#include "Engine/World.h"
#include "Character/Mannequin.h"
#include "DrawDebugHelpers.h"
#include "ActorPool.h"
#include "AI/Navigation/NavigationSystem.h"
#include "GameFramework/Pawn.h"

// Sets default values
ATile::ATile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Defaults 
	NavigationBoundsOffset = FVector(2000, -2000.0, 0);
	MinExtent = FVector(0, -4000, 0);
	MaxExtent = FVector(4000, 0, 0);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

}
void ATile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (Pool != nullptr && NavMeshBoundsVolume != nullptr) {
	Pool->Return(NavMeshBoundsVolume);	
	}

}

void ATile::SetPool(UActorPool* InPool) {
	
	
	UE_LOG(LogTemp, Warning, TEXT("[%s] Setting Pool %s"), *(this->GetName()), *(InPool->GetName()) )

	Pool = InPool;

	PositionNavMeshBoundsVolume();
}

void ATile::PositionNavMeshBoundsVolume()
{	
	NavMeshBoundsVolume = Pool->Checkout();	
	if (NavMeshBoundsVolume == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("[%s] Not enough actors in pool."), *GetName());
		return;
	}
	UE_LOG(LogTemp, Warning, TEXT("[%s] Checkout:{%s}"), *GetName(), *NavMeshBoundsVolume->GetName());
		
	NavMeshBoundsVolume->SetActorLocation(GetActorLocation() + NavigationBoundsOffset);
	GetWorld()->GetNavigationSystem()->Build();
}


// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ATile::FindEmptyLocation(FVector &OutLocation,float Radius) {

	FBox Bounds(MinExtent, MaxExtent);
	const int MAX_ATTEMPTS = 20;
	for (size_t i = 0; i < MAX_ATTEMPTS; i++) {
		FVector CandidatePoint = FMath::RandPointInBox(Bounds);
		if (CanSpawnAtLocation(CandidatePoint, Radius)) {
			OutLocation = CandidatePoint; //returns only when CanSpawn is true as debugcapsule touches nothing
			return true;
		}
	}
	return false;
}

template<class T> // put this above other references to this method to avoid compile error
void ATile::RandomlyPlaceActors(TSubclassOf<T> ToSpawn, int MinSpawn, int MaxSpawn, float MinScale, float MaxScale, float Radius) 
{
	int NumberToSpawn = FMath::RandRange(MinSpawn, MaxSpawn);
	for (size_t i = 0; i < NumberToSpawn; i++) { //size_t can hold the largest possible size

		FSpawnPosition SpawnPosition;

		SpawnPosition.Scale = FMath::RandRange(MinScale, MaxScale);
		bool found = FindEmptyLocation(SpawnPosition.Location, Radius*SpawnPosition.Scale);
		if (found) {
			SpawnPosition.Rotation = FMath::RandRange(-180.f, 180.f);
			PlaceActor(ToSpawn, SpawnPosition);
		}
		//	FVector SpawnPoint = FMath::RandPointInBox(Bounds);  //Generate anywhere in between box
		//UE_LOG(LogTemp, Warning, TEXT("SpawnPoint: %s"), *SpawnPoint.ToCompactString())
	}
	
}





void ATile::PlaceActors(TSubclassOf<AActor> ToSpawn, int MinSpawn, int MaxSpawn, float Radius, float MinScale, float MaxScale) {
	/*
	TArray<FSpawnPosition> SpawnPositions = RandomSpawnPositions(MinSpawn, MaxSpawn, MinScale, MaxScale, Radius);

	for (FSpawnPosition SpawnPosition : SpawnPositions) // loop and iterate over anything of struct SpawnPosition in the TArray SpawnPositions
	{	
		PlaceActor(ToSpawn, SpawnPosition);
	}
	*/
	//Generate anywhere in between box
	RandomlyPlaceActors(ToSpawn, MinSpawn,MaxSpawn,MinScale,MaxScale,Radius);
}
void ATile::PlaceActor(TSubclassOf<AActor> ToSpawn, const FSpawnPosition& SpawnPosition) {
	AActor* Spawned = GetWorld()->SpawnActor<AActor>(ToSpawn);
	if (Spawned) {
		Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
		Spawned->SetActorRelativeLocation(FVector(SpawnPosition.Location));
		Spawned->SetActorRotation(FRotator(0, SpawnPosition.Rotation, 0));
	//	Spawned->SetActorScale3D(FVector(SpawnPosition.Scale));
	}
}
void ATile::PlaceAIPawns(TSubclassOf<APawn> ToSpawn, int MinSpawn, int MaxSpawn, float Radius)
{
	RandomlyPlaceActors(ToSpawn, MinSpawn, MaxSpawn, 1, 1, Radius);
}
void ATile::PlaceActor(TSubclassOf<APawn> ToSpawn, const FSpawnPosition& SpawnPosition) {
	
	FRotator Rotation =FRotator(0, SpawnPosition.Rotation, 0);
	APawn* Spawned = GetWorld()->SpawnActor<APawn>(ToSpawn,SpawnPosition.Location,Rotation);

	if (Spawned) {

		Spawned->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
		Spawned->SetActorRelativeLocation(FVector(SpawnPosition.Location.X, SpawnPosition.Location.Y, 105.f));
		//		Spawned->SetActorScale3D(FVector(SpawnPosition.Scale));
		Spawned->Tags.Add(FName("Enemy"));
		Spawned->SpawnDefaultController();
	}
}


bool ATile::CanSpawnAtLocation(FVector Location, float Radius) {
	FHitResult HitResult;

	FVector GlobalLocation = ActorToWorld().TransformPosition(Location);
	bool HasHit = GetWorld()->SweepSingleByChannel(
		HitResult,
		GlobalLocation,
		GlobalLocation,
		FQuat::Identity,
		ECollisionChannel::ECC_GameTraceChannel2, //trace channels from project settings in defautEngine.ini
		FCollisionShape::MakeSphere(Radius)
	);
//	FColor ResultColor = HasHit ? FColor::Red : FColor::Green; // A ? B : C means if true, return B, if false return C		 
//	DrawDebugCapsule(GetWorld(), GlobalLocation, 0, Radius, FQuat::Identity, ResultColor, true, 100);

//	UE_LOG(LogTemp,Error,TEXT("Capsul spawnddd"))
	return !HasHit; //return true if sphere touches nothing
}

