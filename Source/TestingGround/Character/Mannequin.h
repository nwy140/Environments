// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Mannequin.generated.h"

class UCameraComponent;
class USkeletalMeshComponent;
class AGun;

UCLASS()
class TESTINGGROUND_API AMannequin : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMannequin();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<class AGun> GunBlueprint; // allow selection of BP Class in Blueprints

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void PullTrigger();
	void PlayerPullTrigger();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	bool GetFiring();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void SetFiring(bool Firing);


	// if you can , try and give a UPROPERTY for every classes in your header , and a UFunction for every method in your header

	UPROPERTY(VisibleAnywhere, Category = Mesh)
	class USkeletalMeshComponent* Mesh1P = nullptr;

	UPROPERTY(VisibleAnywhere)
	class UCameraComponent* FPCamera = nullptr;

	UPROPERTY(VisibleAnywhere)
	class AGun* Gun = nullptr;


	UPROPERTY(EditDefaultsOnly)
	int RapidFire = 3;
	
	UPROPERTY(EditDefaultsOnly)
	bool Firing = false;



	virtual void UnPossessed() override;


};
