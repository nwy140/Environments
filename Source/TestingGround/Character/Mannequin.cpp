// Fill out your copyright notice in the Description page of Project Settings.

#include "Mannequin.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneCaptureComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Weapons/Gun.h"
#include "GameFramework/Actor.h"

class AGun; 

// Sets default values
AMannequin::AMannequin()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FPCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FPCamera"));
	FPCamera->SetupAttachment(GetCapsuleComponent());
	FPCamera->SetRelativeLocation(FVector(10.439999, 1.75, 64.0));
	FPCamera->bUsePawnControlRotation = true;
	
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FPArms"));
	Mesh1P->SetupAttachment(FPCamera);
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);
	
}

// Called when the game starts or when spawned
void AMannequin::BeginPlay()
{
	Super::BeginPlay();
	if (GunBlueprint == NULL) {
		UE_LOG(LogTemp, Warning, TEXT("Gun Blueprint Missing"))
			return;
	}

	Gun = GetWorld()->SpawnActor<AGun>(GunBlueprint);

	if (IsPlayerControlled()){
		Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));
		Gun->SetActorRelativeRotation(FRotator(0, -17, 0));
	}
	else {
		Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint_0"));
	}
	Gun->AnimInstance1P = Mesh1P->GetAnimInstance();
	Gun->AnimInstance3P = GetMesh()->GetAnimInstance();



	if (InputComponent != nullptr) {
		InputComponent->BindAction("Fire", IE_Pressed, this, &AMannequin::PlayerPullTrigger);
	}
}

// Called every frame
void AMannequin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMannequin::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);


}

void AMannequin::PullTrigger() {
	if (Gun == NULL) {
		UE_LOG(LogTemp, Warning, TEXT("No Gun Spawned"))
		return;
	}
	Firing = true;
	Gun->OnFire();
//	UE_LOG(LogTemp,Warning,TEXT("Firing"))
}
void AMannequin::PlayerPullTrigger() {
//	for (int i = 0; i<RapidFire; i++) { //shoot 4 rapid fire // add rapid fire behavior later
		PullTrigger();
//	}
}

bool AMannequin::GetFiring()
{
	return Firing;
}

void AMannequin::SetFiring(bool Firing)
{
	this->Firing = Firing;

}


void AMannequin::UnPossessed(){
	Super::UnPossessed(); //try to call super everytime you override a method to avoid bugs
	if (!ensure(Gun)) {
		UE_LOG(LogTemp, Error, TEXT("Gun Not Found"));
		return;
	}
	Gun->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint_0"));
}



//Features to add in Game
/*
- Stealth Combo multiplayer
if level cleared in stealth
multiplier x1.2 + 0.2 each clear level

*/