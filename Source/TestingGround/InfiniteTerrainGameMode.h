// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "TestingGroundGameMode.h"
#include "InfiniteTerrainGameMode.generated.h"

/**
*
*/
UCLASS()
class TESTINGGROUND_API AInfiniteTerrainGameMode : public ATestingGroundGameMode
{
	GENERATED_BODY()

public:
	AInfiniteTerrainGameMode();

	UFUNCTION(BlueprintCallable, Category = "Bounds Pool")
	void PopulateBoundsVolumePool();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pool")
	class UActorPool* NavMeshBoundsVolumePool = nullptr;

	
	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	int Score = 0;

	UFUNCTION(BlueprintCallable, Category = "Spawning")
	int NewTileConquered();
	
	UFUNCTION(BlueprintCallable, Category = "Spawning")
	int GetScore();
	

protected:

private:
	void AddToPool(class ANavMeshBoundsVolume *VolumeToAdd); //declare class here so don't need to use #include or forward declare

};