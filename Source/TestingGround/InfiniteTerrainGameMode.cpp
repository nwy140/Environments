// Fill out your copyright notice in the Description page of Project Settings.

#include "InfiniteTerrainGameMode.h"	
#include "TestingGroundGameMode.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "EngineUtils.h"
#include "ActorPool.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode() {
	NavMeshBoundsVolumePool = CreateDefaultSubobject<UActorPool>(FName("Nav Mesh Bounds Volume Pool"));
}
void AInfiniteTerrainGameMode::PopulateBoundsVolumePool() {

	auto VolumeIterator = TActorIterator<ANavMeshBoundsVolume>(GetWorld()); //Get all actors/volumes in the scene
	while (VolumeIterator) { // if there is a new actoriterator/volumeiterator
	AddToPool(*VolumeIterator);
	++VolumeIterator; //++ActorIterator // add to iterator first then set it// ActorIterator++ // set it then add to iterator

	}

}	

void AInfiniteTerrainGameMode::AddToPool(ANavMeshBoundsVolume *VolumeToAdd) {
	UE_LOG(LogTemp, Warning, TEXT("Found NavMeshBoundsVolume: %s"), *VolumeToAdd->GetName());
	NavMeshBoundsVolumePool->Add(VolumeToAdd);

} // Alternate use of finding all actors of that // class UGameplayStatics::GetAllActorsOfClass(GetWorld(), YourClass::StaticClass(), FoundActors);


int AInfiniteTerrainGameMode::NewTileConquered() {
	Score++;
	UE_LOG(LogTemp, Warning, TEXT("Score: %i"), Score)
		return Score;
}

int AInfiniteTerrainGameMode::GetScore() {

	return Score;
}

